(function(exp) {
  var audioCtx = new AudioContext();
  
  exp.pure = function (hz, mag, buffer, start, stop) {
    start *= audioCtx.sampleRate;
    stop *= audioCtx.sampleRate;
    var mult = hz * 2 * Math.PI / audioCtx.sampleRate;
    for (var i = start; i < stop; ++i) {
      buffer[i] += Math.sin(mult * i) * mag;
    }
  };
  
  exp.string = function (nodes, weight, mag, dur, buffer, start, stop) {
    start *= audioCtx.sampleRate;
    stop *= audioCtx.sampleRate;
    start = Math.floor(start);
    stop = Math.ceil(stop);
    mag *= 10;
    var damp = 1 - 1 / (dur * 100);
    var factor = Math.sqrt(1 / weight);
    var pos = new Array(nodes);
    for (var i = 0; i < nodes/2; ++i) {
      pos[i] = i / nodes;
    }
    for (var i = nodes/2; i < nodes; ++i) {
      pos[i] = (nodes - i) / nodes;
    }
    var vel = new Array(nodes);
    for (var i = 0; i < nodes; ++i)
      vel[i] = 0;
    for (var xx = start; xx < stop; ++xx) {
      var r = 0;
      for (var i = 1; i < nodes - 1; ++i) {
        var ddx = (pos[i-1] + pos[i+1] - pos[i] * 2) * factor;
        vel[i] += ddx;
      }
      for (var i = 1; i < nodes - 1; ++i) {
        vel[i] *= damp;
        var dx = vel[i];
        r += dx;
        pos[i] += dx;
      }
      buffer[xx] += r * mag;
    }
  }
  
  exp.createBuffer = function(dur) {
    var channels = 1;
    var frameCount = audioCtx.sampleRate * dur;
    frameCount = Math.ceil(frameCount);
    return audioCtx.createBuffer(channels, frameCount, audioCtx.sampleRate);
  }
  
  exp.play = function(buffer) {
    var source = audioCtx.createBufferSource();
    source.buffer = buffer;
    source.connect(audioCtx.destination);
    source.start(0);
  };
})(gen = {});

